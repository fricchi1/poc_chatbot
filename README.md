# Chatbot PoC

This project is a proof of concept (PoC) for a script that scrapes a URL or a list of URLs and stores the scraped data in a vectorial database. The stored data can then be used to respond to questions about the scraped pages.

## Features

- Scrape a single URL or a list of URLs from a text file.
- Store the scraped data in a vectorial database.
- Query the stored data using a language model to answer questions about the content.

## Usage

### Command Line Arguments

The script accepts the following optional arguments:

- `-h, --help`: Show the help message and exit.
- `-u URL, --url URL`: URL of the website to be scraped.
- `-f FILE, --file FILE`: Text file with the URLs of the websites to be scraped.
- `-q QUESTION, --question QUESTION`: Question to ask the language model (LLM) about the scraped data.
- `-F FORCE, --force FORCE`: Force the scraping even if data already exists.

### Example Commands

#### Scrape a Single URL

To scrape a single URL and store the data in the vectorial database, use the following command:

```sh
python chatbot_script.py -u https://www.tmimza.com/ -q "Que tipo de servicios ofrece la empresa?"
```
Scrape Multiple URLs from a File
```sh
python chatbot_script.py -f example/um_urls.txt -q "Podrías darme información de la carrera de ingeniería en informática?"
```


### Requirements
Python 3.9.1
Required Python packages listed in requirements.txt
A new personal.py file with GEMINI_API_KEY constant value
