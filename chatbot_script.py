import argparse
import logging
import re
import sys
from typing import List

import chromadb
from llama_index.core import ChatPromptTemplate
from llama_index.core import PromptTemplate
from llama_index.core import ServiceContext
from llama_index.core import Settings
from llama_index.core import StorageContext
from llama_index.core import VectorStoreIndex
from llama_index.core.llms import ChatMessage
from llama_index.core.llms import MessageRole
from llama_index.core.schema import Document
from llama_index.embeddings.huggingface import HuggingFaceEmbedding
from llama_index.llms.gemini import Gemini
from llama_index.llms.gemini.base import GEMINI_MODELS
from llama_index.llms.ollama import Ollama
from llama_index.readers.web import BeautifulSoupWebReader
from llama_index.readers.web import ReadabilityWebPageReader
from llama_index.readers.web import WholeSiteReader
from llama_index.vector_stores.chroma import ChromaVectorStore

from personal import GEMINI_API_KEY

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)

logger.addHandler(handler)

formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)


def sanitize_url(url) -> str:
    # Remove the protocol (http:// or https://)
    url = re.sub(r"^https?://", "", url)
    # Remove the "www." prefix
    url = re.sub(r"^www\.", "", url)
    # Remove anything after the domain
    url = re.split(r"[/?#]", url)[0]
    return url


def read_urls_from_file(file_path: str) -> List[str]:
    # Check if the file extension is .txt
    if not file_path.endswith(".txt"):
        raise ValueError("The file is not a text file (.txt)")

    try:
        # Open the file in read mode
        with open(file_path, "r") as file:
            # Read the content of the file and split it by lines
            urls = file.read().splitlines()
            return urls
    except FileNotFoundError:
        # If the file does not exist, raise an error
        raise FileNotFoundError("The file does not exist")


def refine_documents(documents: List[Document]):
    """summarizer_prompt = (
        "You are an expert summarizer and content creator. Based on the document you just read, please provide the following:"
        "A concise and engaging title that reflects the main theme or purpose of the document."
        "A brief and informative introduction (2-3 sentences) that summarizes the key points, sets the tone, and outlines the document's purpose or value for the reader."
        "Make sure the title is attention-grabbing and the introduction is clear and professional, capturing the essence of the content."
        "Pay close attention to the URL, as it may contain important or contextual information related to the content you are summarizing."
        "---------------------\n"
        "url:"
        "https://um.edu.ar/carreras/ingenieria-en-informatica/"
        "parsed web information:"
        "{context_str}"
        "---------------------\n"
    )"""
    information_processing_prompt_str = (
        "Given the following parsed web page content, extract all information you can to create a comprehensive document."
        "Exclude any text related to web page structure,metadata, or HTML tags."
        "if it contains menu items or navigation bars that do not provide relevant information related to the topic being scraped."
        "Clean the the text by removing or ignoring unnecessary escape sequences like \\n, \\r, and \\t."
        "Keep only those that are essential for maintaining the correct structure and formatting."
        "This document should be designed to prepare an employee to respond to any inquiries about the information on the web page"
        "---------------------\n"
        "parsed web information:"
        "{context_str}"
        "---------------------\n"
        "The output should be well-organized, clear, and professional,"
        "ensuring that an employee can easily understand and use the information to answer questions about the company."
    )
    review_and_refine_prompt = (
        "Given the following information you have created and the original source you have read from:"
        "\n\n"
        "1. Review both texts carefully to ensure the document you created aligns with the original source."
        "2. Check if the document is accurate, complete, and well-structured."
        "3. Make any necessary tweaks to improve clarity and professionalism."
        "4. Add any missing parts that are essential to the document's purpose."
        "5. Remove any content that is unnecessary, irrelevant, or redundant."
        "\n\n"
        "Your goal is to refine the document so it is polished, precise, and ready for use."
        "---------------------\n"
        "Original Source:\n"
        "{original_source}\n"
        "---------------------\n"
        "Created Document:\n"
        "{created_document}\n"
        "---------------------\n"
    )

    prompt_template = PromptTemplate(information_processing_prompt_str)
    second_prompt_template = PromptTemplate(review_and_refine_prompt)

    refined_documents = []
    for document in documents:
        # llm = Gemini(api_key=GEMINI_API_KEY, model_name=GEMINI_MODELS[6])
        llm = Ollama(model="gemma2:2b", request_timeout=120.0)
        messages = prompt_template.format_messages(context_str=document.text)
        first_response = llm.chat(messages)

        new_messages = second_prompt_template.format_messages(
            original_source=document.text,
            created_document=first_response.message.content,
        )
        second_response = llm.chat(new_messages)

        refined_documents.append(
            Document(
                text=second_response.message.content,
                id_=document.id_,
                extra_info=document.extra_info,
            )
        )
    return refined_documents


def get_options() -> dict[str, any]:
    parser = argparse.ArgumentParser(
        description="This script serves as a PoC for the ChatFromScratch project."
    )
    parser.add_argument(
        "-u", "--url", help="URL to the website to be scraped.", required=False
    )
    parser.add_argument(
        "-f",
        "--file",
        help="Txt file with the URLs to the website to be scraped.",
        required=False,
    )
    parser.add_argument("-q", "--question", help="Question to ask LLM.", required=True)
    parser.add_argument(
        "-F",
        "--force",
        help="Force scraping the website even if it’s already stored.",
        required=False,
        default=False,
    )

    args = parser.parse_args()

    return args


def run_poc(domain: str, urls: List[str], question: str, force_scrapping: bool = False):
    embed_model = HuggingFaceEmbedding(model_name="BAAI/bge-small-en-v1.5")

    # llm = Gemini(api_key=GEMINI_API_KEY, model_name=GEMINI_MODELS[6])
    llm = Ollama(model="gemma2:2b", request_timeout=120.0)

    Settings.llm = llm
    Settings.embed_model = embed_model

    # svc = ServiceContext.from_defaults(embed_model=embed_model, llm=llm)

    logger.info("Setting up the database")

    db = chromadb.PersistentClient(path="./chroma_db_HF")
    chroma_client = chromadb.EphemeralClient()
    # db = chromadb.HttpClient(host="127.0.0.1", port=8001)

    try:
        logger.info(f"Checking for an existing {domain} collection")
        chroma_collection = db.get_collection(domain)
        vector_store = ChromaVectorStore(chroma_collection=chroma_collection)
        index = VectorStoreIndex.from_vector_store(
            vector_store,
        )
    except Exception:
        logger.info(f"The {domain} collection isn't loaded")
        logger.info(f"Proceed to scrape the web")
        loader = BeautifulSoupWebReader()
        documents = loader.load_data(urls=urls)
        logger.info(f"Refining scraped documents")
        documents = refine_documents(documents)
        logger.info(f"Creating {domain} collection")
        chroma_collection = db.create_collection(domain)
        vector_store = ChromaVectorStore(chroma_collection=chroma_collection)
        storage_context = StorageContext.from_defaults(vector_store=vector_store)
        index = VectorStoreIndex.from_documents(
            documents,
            storage_context=storage_context,
        )

    query_engine = index.as_chat_engine()
    logger.info(f"Quering to LLM {llm.model}")
    response = query_engine.chat(question)
    print("================================================")
    print(response)
    print("================================================")


def run():
    opts = get_options()

    domain = None
    urls = []
    question = opts.question

    if opts.url:
        domain = sanitize_url(opts.url) + "-db"
        urls.append(opts.url)

    if opts.file:
        urls = read_urls_from_file(opts.file)
        domain = sanitize_url(urls[0]) + "-db"

    run_poc(domain, urls, question, opts.force)


run()
